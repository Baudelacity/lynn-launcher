package com.baudelacity.lynn;


import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;


public class ApplicationSettings extends Activity {

	SharedPreferences preferences;

	SeekBar icon_size_seekbar, text_size_seekbar, column_width_seekbar, column_width_seekbar2;
	TextView icon_size_text, text_size_text, column_width_text, column_width_text2;
	int icon_size_value = 2, text_size_value = 2, column_width_value = 4;
	int column_width_value2 = 7;
	static int [] Settings;
	protected void onCreate(Bundle savedInstanceState) {

		if (!isTablet()) {
			if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				//Adapter_and_handler();
			}
		}
			super.onCreate(savedInstanceState);


			setContentView(R.layout.application_settings);

			ImageView back_button = (ImageView) findViewById(R.id.back_button);
			back_button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
					overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
				}
			});

			preferences = PreferenceManager.getDefaultSharedPreferences(this);


			//==============================================================================
			icon_size_seekbar = (SeekBar) findViewById(R.id.icon_size_seekbar);
			text_size_seekbar = (SeekBar) findViewById(R.id.text_size_seekbar);
			column_width_seekbar = (SeekBar) findViewById(R.id.column_width_seekbar);
			column_width_seekbar2 = (SeekBar) findViewById(R.id.column_width_seekbar2);

			icon_size_text = (TextView) findViewById(R.id.icon_size_text);
			text_size_text = (TextView) findViewById(R.id.widgets_text);
			column_width_text = (TextView) findViewById(R.id.column_width_text);
			column_width_text2 = (TextView) findViewById(R.id.column_width_text2);
			TextView colviddthtext = (TextView) findViewById(R.id.ColWidthText);

			if (!isTablet()) {
				column_width_seekbar2.setVisibility(View.GONE);
				column_width_text2.setVisibility(View.GONE);
				colviddthtext.setVisibility(View.GONE);


			}
			//==============================================================================

			preferences = this.getSharedPreferences(ApplicationListGrid.PREFERENCES_NAME, MODE_PRIVATE);
			if (isTablet()) {
				Settings = getResources().getIntArray(R.array.tablet);
			} else {
				Settings = getResources().getIntArray(R.array.phone);
			}
			column_width_value = preferences.getInt("column_width_value", Settings[0]);
			column_width_value2 = preferences.getInt("column_width_value_land", Settings[1]);
			icon_size_value = preferences.getInt("icon_size_value", Settings[2]);
			text_size_value = preferences.getInt("text_size_value", Settings[3]);

			//==============================================================================
			icon_size_seekbar.setProgress(icon_size_value - 1);
			icon_size_text.setText(Integer.toString(icon_size_value * 50 + 50) + "%");

			icon_size_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					icon_size_text.setText(Integer.toString((progress + 1) * 50 + 50) + "%");
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {

				}
			});

			text_size_seekbar.setProgress(text_size_value - 1);
			text_size_text.setText(Integer.toString(text_size_value * 50 + 50) + "%");
			text_size_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					text_size_text.setText(Integer.toString((progress + 1) * 50 + 50) + "%");
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {

				}
			});


			column_width_seekbar.setProgress(column_width_value - 1);
			column_width_text.setText(Integer.toString(column_width_value));
			column_width_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					column_width_text.setText(Integer.toString(progress + 1));
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {

				}
			});


			column_width_seekbar2.setProgress(column_width_value2 - 1);
			column_width_text2.setText(Integer.toString(column_width_value2));
			column_width_seekbar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

				@Override
				public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
					column_width_text2.setText(Integer.toString(arg1 + 1));
				}

				@Override
				public void onStartTrackingTouch(SeekBar arg0) {
				}

				@Override
				public void onStopTrackingTouch(SeekBar arg0) {
				}
			});

		}

	public void onPause() {
		super.onPause();
		SaveSettings();
		overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
	
		
	}
	public void SaveSettings() {
		SharedPreferences.Editor preferencesEditor = preferences.edit();
		column_width_value = column_width_seekbar.getProgress();
		column_width_value2 = column_width_seekbar2.getProgress();
		icon_size_value = icon_size_seekbar.getProgress();
		text_size_value = text_size_seekbar.getProgress();
		column_width_value = column_width_value+1;
		column_width_value2 = column_width_value2+1;
		icon_size_value = icon_size_value+1;
		text_size_value = text_size_value+1;
		preferencesEditor.putInt("icon_size_value", icon_size_value);
		preferencesEditor.putInt("text_size_value", text_size_value);
		preferencesEditor.putInt("column_width_value", column_width_value);
		preferencesEditor.putInt("column_width_value_land", column_width_value2);
		preferencesEditor.commit();
		ApplicationListGrid.set_pacs(true);
		
		
	}
	private boolean isTablet() {
		return (this.getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK)
				>= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);


	}

}
