package com.baudelacity.lynn;

        import android.content.ContentValues;
        import android.content.Context;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;

        import java.util.LinkedList;
        import java.util.List;

/**
 * Created by Krzysztof on 10.06.2016.
 */
public class SQL extends SQLiteOpenHelper {

    public SQL (Context context) {
        super(context, "app_list_database.db", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table app_data(" +
                        "nr integer primary key autoincrement," +
                        "package text," +
                        "label text," +
                        "original_label text," +
                        "icon_hex text," +
                        "original_icon_hex text);" +
                        "");


    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addLauncher (Icon_Handler launcher) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues wartosci = new ContentValues();
        wartosci.put("package", launcher.getPackage_name());
        wartosci.put("label", launcher.getLabel());
        wartosci.put("icon_hex", launcher.getIcon_hex());
        wartosci.put("original_label", launcher.getOriginal_label());
        wartosci.put("original_icon_hex", launcher.getOriginal_icon_hex());
        db.insertOrThrow("app_data", null,wartosci);
        db.close();
        this.close();
    }

    public List<Icon_Handler> giveAll() {
        List<Icon_Handler> app_list = new LinkedList<Icon_Handler>();
        String[] kolumny = {"nr","package","label","original_label", "icon_hex", "original_icon_hex"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor kursor = db.query("app_data",kolumny,null,null,null,null,null);
        while(kursor.moveToNext()) {
            Icon_Handler launcher = new Icon_Handler();
            launcher.setNr(kursor.getLong(0));
            launcher.setPackage_name(kursor.getString(1));
            launcher.setLabel(kursor.getString(2));
            launcher.setOriginal_label(kursor.getString(3));
            launcher.setIcon_hex(kursor.getString(4));
            launcher.setOriginal_icon_hex(kursor.getString(5));
            app_list.add(launcher);
        }
        kursor.close();
        db.close();
        this.close();
        return app_list;
    }
    public void deleteLauncher (int id) {
        SQLiteDatabase db = getWritableDatabase();
        String[] argumenty = {""+id};
        db.delete("app_data", "nr=?", argumenty);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "app_data" + "'");
        db.close();
        this.close();

    }
    public void updateLauncher(Icon_Handler launcher) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("package", launcher.getPackage_name());
        values.put("label", launcher.getLabel());
        values.put("icon_hex", launcher.getIcon_hex());
        values.put("original_label", launcher.getOriginal_label());
        values.put("original_icon_hex", launcher.getOriginal_icon_hex());
        String args[] = {launcher.getNr()+""};
        db.update("app_data", values, "nr=?",args);
        db.close();
        this.close();
    }
    public Icon_Handler giveLauncher(int nr) {
        Icon_Handler launcher = new Icon_Handler();
        SQLiteDatabase db = getReadableDatabase();
        String[] kolumny = {"nr","package","label","original_label", "icon_hex", "original_icon_hex"};
        String args[] = {nr+""};
        Cursor kursor = db.query("app_data",kolumny," nr=?",args,null,null,null);
        if(kursor!=null) {
            kursor.moveToFirst();
            launcher.setNr(kursor.getLong(0));
            launcher.setPackage_name(kursor.getString(1));
            launcher.setLabel(kursor.getString(2));
            launcher.setOriginal_label(kursor.getString(3));
            launcher.setIcon_hex(kursor.getString(4));
            launcher.setOriginal_icon_hex(kursor.getString(5));
        }
        kursor.close();
        db.close();
        this.close();
        return launcher;
    }

    public Boolean searchKeyString(String key) {
        Boolean rtn = false;
        // Log.d("searchKeyString","app_launcher");

        // Select All Query
        String selectQuery = "select * from app_data where package=\"" + key + "\"";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            rtn = true;

            //rtn = "Wynik:" + cursor.getInt(0) + ", " + cursor.getString(1) + ", " + cursor.getString(2) + ", " + cursor.getString(3);
        }
        cursor.close();
        db.close();
        //Log.d("searchKeyString","finish search");

        return rtn;
    }
}

