package com.baudelacity.lynn;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerAdapter extends BaseAdapter {

	Context context;
	Icon_Handler[] handlerForAdapter;
	public static final String PREFERENCES_NAME = "Settings";
	public SharedPreferences preferences;
	int icon_size_value;
	short icon_value[] = {0,54,67,79};
	short font_value[] = {0,12,15,18};
	String font;
	DrawerAdapter (Context c, Icon_Handler[] handler) {
		context = c;
		handlerForAdapter = handler;
		
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return handlerForAdapter.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	static class ViewHolder {
		TextView text;
		ImageView icon;
		ImageView background;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.drawer_item, null);
			icon_size_value = ApplicationListGrid.icon_size_value;
			icon_size_value = ApplicationListGrid.icon_size_value;
			
			viewHolder = new ViewHolder();
			viewHolder.icon = (ImageView)convertView.findViewById(R.id.icon_image);
			viewHolder.text = (TextView)convertView.findViewById(R.id.icon_text);
			int w = (int) (icon_value[ApplicationListGrid.icon_size_value] * context.getResources().getDisplayMetrics().density + 0.5f);
			
			viewHolder.icon.getLayoutParams().width = w;
			viewHolder.icon.getLayoutParams().height = w;


			viewHolder.text.setTextAppearance(context, R.style.NokiaPure);



			viewHolder.text.setTextSize(font_value[ApplicationListGrid.text_size_value]);
			viewHolder.text.setTypeface(ApplicationListGrid.typeface);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
			
		}
		viewHolder.text.setText(handlerForAdapter[position].getLabel());
		viewHolder.icon.setImageDrawable(handlerForAdapter[position].getIcon());
		return convertView;
	}
}
