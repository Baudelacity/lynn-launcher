package com.baudelacity.lynn;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerLongClickListener implements OnItemLongClickListener {
	Context mContext;
	Icon_Handler[] handlerForAdapter;
	PackageManager pmForListener;
	public static String applabel;
	SharedPreferences preferences;
	public DrawerLongClickListener(Context ctxt, Icon_Handler[] handler, PackageManager pm) {
		mContext = ctxt;
		handlerForAdapter=handler;
		pmForListener=pm;
	}

	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int pos,
								   long arg3) {
		ApplicationListGrid.AppLabel = handlerForAdapter[pos].getLabel();
		ApplicationListGrid.PackageName = handlerForAdapter[pos].getPackage_name();
		ApplicationListGrid.AppIcon = handlerForAdapter[pos].getOriginal_icon();
		ApplicationListGrid.EditedAppIcon = handlerForAdapter[pos].getIcon();
		ApplicationListGrid.OriginalAppLabel = handlerForAdapter[pos].getOriginal_label();
		applabel = handlerForAdapter[pos].getLabel();
		ApplicationListGrid.selected_ID = (short) pos;


		final Dialog dialog = new Dialog(mContext);
		dialog.setContentView(R.layout.dialog_view);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.getWindow().setDimAmount(0.85f);
		TextView header = (TextView) dialog.findViewById(R.id.dialog_header);
		Button Option1 = (Button) dialog.findViewById(R.id.option_1);
		Button Option2 = (Button) dialog.findViewById(R.id.option_2);
		Button Option3 = (Button) dialog.findViewById(R.id.option_3);
		Button Dismiss = (Button) dialog.findViewById(R.id.dismiss_button);
		ImageView icon = (ImageView) dialog.findViewById(R.id.dialog_icon);

		Option1.setText(mContext.getResources().getText(R.string.details));
		Option2.setText(mContext.getResources().getText(R.string.edit_3));
		Option3.setText(mContext.getResources().getText(R.string.remove));
		Dismiss.setText(mContext.getResources().getText(R.string.dismiss));
		icon.setImageDrawable(ApplicationListGrid.EditedAppIcon);
		header.setText(ApplicationListGrid.AppLabel);

		Option1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Uri packageURI = Uri.parse("package:" + ApplicationListGrid.PackageName);
				Intent showInfo = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
				showInfo.setData(packageURI);
				mContext.startActivity(showInfo);
				((Activity) mContext).overridePendingTransition(MainActivity.transition_in_1, MainActivity.transition_out_1);
				dialog.dismiss();
			}
		});
		Option2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				ApplicationListGrid.editor = true;
				Intent icon_editor = new Intent(mContext, icon_editor.class);
				mContext.startActivity(icon_editor);
				dialog.dismiss();
			}
		});
		Option3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				final Uri packageURI2 = Uri.parse("package:" + ApplicationListGrid.PackageName);
				Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI2);

				mContext.startActivity(uninstallIntent);
				((Activity) mContext).overridePendingTransition(MainActivity.transition_in_1, MainActivity.transition_out_1);
				dialog.dismiss();

			}
		});
		Dismiss.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();

		/*
		final CharSequence[] items = {
				mContext.getResources().getText(R.string.details),
				mContext.getResources().getText(R.string.remove),
				mContext.getResources().getText(R.string.edit_3)
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AppTheme_Dialog);
		builder.setIcon(ApplicationListGrid.EditedAppIcon);
		builder.setTitle(ApplicationListGrid.AppLabel);
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch(item) {
					case 0: {
						Uri packageURI = Uri.parse("package:" + ApplicationListGrid.PackageName);
						Intent showInfo = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
						showInfo.setData(packageURI);
						mContext.startActivity(showInfo);
						((Activity) mContext).overridePendingTransition(MainActivity.transition_in_1, MainActivity.transition_out_1);
						return;
					}
					case 1: {
						/*final Uri packageURI2 = Uri.parse("package:" + ApplicationListGrid.PackageName);
						Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI2);
						mContext.startActivity(uninstallIntent);

						final Uri packageURI2 = Uri.parse("package:" + ApplicationListGrid.PackageName);
						Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI2);

						mContext.startActivity(uninstallIntent);
						((Activity) mContext).overridePendingTransition(MainActivity.transition_in_1, MainActivity.transition_out_1);
						//int result = 0;
						//((Activity) mContext).startActivityForResult(uninstallIntent, result);
						return;
					}

					case 2: {
						ApplicationListGrid.editor = true;
						Intent icon_editor = new Intent(mContext, icon_editor.class);
						mContext.startActivity(icon_editor);
						((Activity) mContext).overridePendingTransition(MainActivity.transition_in_1, MainActivity.transition_out_1);
					}


				}

			}
		});
		builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
			//	Log.d("Test", "Test");
			}
		});
		AlertDialog alert = builder.create();
		alert.show(); */
		return true;
	}
	public void onActivityResult (int requestCode, int resultCode, Intent data) {
	}

}
