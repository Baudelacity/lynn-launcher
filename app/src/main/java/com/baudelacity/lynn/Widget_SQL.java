package com.baudelacity.lynn;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Krzysztof on 22.11.2016.
 */
public class Widget_SQL extends SQLiteOpenHelper {

    public Widget_SQL (Context context) {
        super(context, "widget_database.db", null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table widget_data(" +
                        "nr integer primary key autoincrement," +
                        "widget_ID integer," +
                        "widget_size integer;" +
                        "");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
