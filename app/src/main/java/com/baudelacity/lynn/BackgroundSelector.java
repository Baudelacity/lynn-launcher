package com.baudelacity.lynn;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;

public class BackgroundSelector extends Activity {  
    Integer hex;
	int Settings [];
    @Override  
    protected void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.icon_grid_layout);
		ImageView back_button = (ImageView) findViewById(R.id.back_button);
		back_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
			}
		});
		if (MainActivity.isTablet(this)) {
			Settings = getResources().getIntArray(R.array.tablet);
		} else {
			Settings = getResources().getIntArray(R.array.phone);
		}
		if (!MainActivity.isTablet(this)) {
			if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				//Adapter_and_handler();
			}
			//Log.d("IsTablet", "Yes, it's a tablet");

		}
            GridView list = (GridView) findViewById(R.id.content);
            final String[] DayOfWeek = {"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
									"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
					"Blue 1", "Blue 2", "Blue 3","Blue 4", "Gray 1",
            							};
            final Integer[] imageId = {
					R.mipmap.material_amber_1, R.mipmap.material_amber_2, R.mipmap.material_amber_3, R.mipmap.material_amber_4, R.mipmap.material_amber_5,
					R.mipmap.material_blue_1, R.mipmap.material_blue_2, R.mipmap.material_blue_3, R.mipmap.material_blue_4, R.mipmap.material_blue_5,
					R.mipmap.material_blue_grey_1, R.mipmap.material_blue_grey_2, R.mipmap.material_blue_grey_3, R.mipmap.material_blue_grey_4, R.mipmap.material_blue_grey_5,
					R.mipmap.material_brown_1, R.mipmap.material_brown_2, R.mipmap.material_brown_3, R.mipmap.material_brown_4, R.mipmap.material_brown_5,
					R.mipmap.material_cyan_1, R.mipmap.material_cyan_2, R.mipmap.material_cyan_3, R.mipmap.material_cyan_4, R.mipmap.material_cyan_5,
					R.mipmap.material_deep_orange_1, R.mipmap.material_deep_orange_2, R.mipmap.material_deep_orange_3, R.mipmap.material_deep_orange_4, R.mipmap.material_deep_orange_5,
					R.mipmap.material_deep_purple_1, R.mipmap.material_deep_purple_2, R.mipmap.material_deep_purple_3, R.mipmap.material_deep_purple_4, R.mipmap.material_deep_purple_5,
					R.mipmap.material_green_1, R.mipmap.material_green_2, R.mipmap.material_green_3, R.mipmap.material_green_4, R.mipmap.material_green_5,
					R.mipmap.material_grey_1, R.mipmap.material_grey_2, R.mipmap.material_grey_3, R.mipmap.material_grey_4, R.mipmap.material_grey_5,
					R.mipmap.material_indigo_1, R.mipmap.material_indigo_2, R.mipmap.material_indigo_3, R.mipmap.material_indigo_4, R.mipmap.material_indigo_5,
					R.mipmap.material_light_blue_1, R.mipmap.material_light_blue_2, R.mipmap.material_light_blue_3, R.mipmap.material_light_blue_4, R.mipmap.material_light_blue_5,
					R.mipmap.material_light_green_1, R.mipmap.material_light_green_2, R.mipmap.material_light_green_3, R.mipmap.material_light_green_4, R.mipmap.material_light_green_5,
					R.mipmap.material_lime_1, R.mipmap.material_lime_2, R.mipmap.material_lime_3, R.mipmap.material_lime_4, R.mipmap.material_lime_5,
					R.mipmap.material_orange_1, R.mipmap.material_orange_2, R.mipmap.material_orange_3, R.mipmap.material_orange_4, R.mipmap.material_orange_5,
					R.mipmap.material_pink_1, R.mipmap.material_pink_2, R.mipmap.material_pink_3, R.mipmap.material_pink_4, R.mipmap.material_pink_5,
					R.mipmap.material_purple_1, R.mipmap.material_purple_2, R.mipmap.material_purple_3, R.mipmap.material_purple_4, R.mipmap.material_purple_5,
					R.mipmap.material_red_1, R.mipmap.material_red_2, R.mipmap.material_red_3, R.mipmap.material_red_4, R.mipmap.material_red_5,
					R.mipmap.material_teal_1, R.mipmap.material_teal_2, R.mipmap.material_teal_3, R.mipmap.material_teal_4, R.mipmap.material_teal_5,
					R.mipmap.material_yellow_1, R.mipmap.material_yellow_2, R.mipmap.material_yellow_3, R.mipmap.material_yellow_4, R.mipmap.material_yellow_5,

			};

            BGPickerAdapter adapter = new BGPickerAdapter(BackgroundSelector.this, DayOfWeek, imageId);
            list.setAdapter(adapter);

		if (MainActivity.isTablet(this)) {
			Settings = getResources().getIntArray(R.array.tablet);
		} else {
			Settings = getResources().getIntArray(R.array.phone);
		}

		list.setNumColumns(Settings[0]);
            list.setOnItemClickListener(new OnItemClickListener() {

    			@Override
    			public void onItemClick(AdapterView<?> parent, View view,
    					int position, long id) {
    				// TODO Auto-generated method stub
    				hex = position;
    				Intent intent=new Intent();
    				intent.putExtra("IMAGE",position);
    				setResult(Activity.RESULT_OK,intent);
    				finish();
					overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
    				}
    			
            	
            }); 
    
    }
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);


	}
}  
