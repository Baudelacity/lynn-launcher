package com.baudelacity.lynn;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;

public class SettingsMenu_OnClick implements OnClickListener {
	Context context;
	
	public SettingsMenu_OnClick(Context c) {
		context = c;
	}
	@Override
	public void onClick(View v) {	
		
		final CharSequence[] items = {
				context.getResources().getText(R.string.add_wdgt),
				context.getResources().getText(R.string.rem_wdgt),
				context.getResources().getText(R.string.settings),
				};
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(R.mipmap.settings_icon_normal);
		builder.setTitle(context.getResources().getText(R.string.options));
		builder.setItems(items, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch(item) {
				case 0: {
				}
				case 1: {
					
				}
				case 2: {
					
				}
				}
				
			}
		});
		
		
		
		AlertDialog alert = builder.create();
		alert.show();
		
	}

}
