package com.baudelacity.lynn;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Krzysztof on 11.08.2016.
 */
public class Credits extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.credits);

        ImageView back_button = (ImageView) findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
            }
        });



    }
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);


    }
}
