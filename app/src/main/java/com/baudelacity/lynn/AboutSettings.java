package com.baudelacity.lynn;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutSettings extends Activity {
	Context c;

	TextView app_version, about;
	ImageView logo, about_logo;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		c = getApplicationContext();
		setContentView(R.layout.about_settings);

		if (!MainActivity.isTablet(c)) {
			if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				//Adapter_and_handler();
			}


			ImageView back_button = (ImageView) findViewById(R.id.back_button);
			back_button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
					overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
				}
			});

			Button credits_button = (Button) findViewById(R.id.button_credits);
			credits_button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(c, Credits.class);
					//Intent intent = new Intent(Credits.class);
					startActivity(intent);
					overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
				}
			});


			logo = (ImageView) findViewById(R.id.Blackmoor_logo);
			about_logo = (ImageView) findViewById(R.id.Logo);

			app_version = (TextView) findViewById(R.id.app_version);
			about = (TextView) findViewById(R.id.about_text);
			about.setText(this.getText(R.string.about));

			PackageInfo pInfo = null;
			try {
				pInfo = getPackageManager().getPackageInfo(this.getPackageName(), 0);
			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}
			String version = pInfo.versionName;
			app_version.setText(getResources().getString(R.string.version) + ": " + version);

			logo.setOnClickListener(new OnClickListener() {
				byte k;

				@Override
				public void onClick(View v) {

					++k;
					if (k == 10) {

						Intent i = new Intent(Intent.ACTION_VIEW,
								Uri.parse("http://twokinds.keenspot.com/"));
						startActivity(i);
					}
				}
			});

			about_logo.setOnClickListener(new OnClickListener() {

											  @Override
											  public void onClick(View arg0) {
												  Intent i = new Intent(Intent.ACTION_VIEW,
														  Uri.parse("http://baudelacity.weebly.com/"));
												  startActivity(i);
											  }

										  }
			);

		}
	}
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);


	}


}
