package com.baudelacity.lynn;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class GeneralSettings extends Activity {
	SharedPreferences preferences;
	public static String font, color;
	int spinner_1_position, spinner_2_position, spinner_3_position;
	Spinner font_spinner, color_spinner, transition_spinner;
	private List<String> font_list, color_list, transition_list;
	static int [] Settings;
	static String [] FONTS, FONT_NAMES;
	
	Button set_wallpaper;
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		ChangeFont.replaceDefaultFont(this, "MONOSPACE", "fonts/nokiapurebold.ttf");
		ChangeFont.replaceDefaultFont(this, "SERIF", "fonts/nokiapure.ttf");
		ChangeFont.replaceDefaultFont(this, "SANS_SERIF", "fonts/nokiapurelight.ttf");
		Settings = getResources().getIntArray(R.array.universal);
        setContentView(R.layout.general_settings);

		FONTS = getResources().getStringArray(R.array.FONTS);
		FONT_NAMES = getResources().getStringArray(R.array.FONT_NAMES);

		ImageView back_button = (ImageView) findViewById(R.id.back_button);
		back_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
			}
		});



		set_wallpaper = (Button) findViewById(R.id.wallpaper_set);
//===========================================================================
        set_wallpaper.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_SET_WALLPAPER);
				startActivity(Intent.createChooser(intent, getResources().getText(R.string.set_wallpaper)));	
			}
        });
        
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        font_spinner = (Spinner) findViewById(R.id.spinner_date_top);
		transition_spinner = (Spinner) findViewById(R.id.spinner_transition);
//===========================================================================
		preferences = this.getSharedPreferences(ApplicationListGrid.PREFERENCES_NAME, Activity.MODE_PRIVATE);
		
		//font = preferences.getString("font", "fonts/roboto.ttf");
		spinner_1_position = preferences.getInt("spinner_1_position", Settings[2]);
		//spinner_2_position = preferences.getInt("spinner_2_position", 0);
		spinner_3_position = preferences.getInt("spinner_3_position", Settings[3]);
		
		font_list = new ArrayList<String>();

		for(int x=0;x<FONT_NAMES.length;x++) {
			font_list.add(FONT_NAMES[x]);
		};


		transition_list = new ArrayList<String>();
		transition_list.add((String) getResources().getText(R.string.anim_1) );
		transition_list.add((String) getResources().getText(R.string.anim_2) );
		transition_list.add((String) getResources().getText(R.string.anim_3) );

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item, font_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        font_spinner.setAdapter(dataAdapter);
        font_spinner.setSelection(spinner_1_position);

		ArrayAdapter<String> dataAdapter_3 = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item, transition_list);
		dataAdapter_3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		transition_spinner.setAdapter(dataAdapter_3);
		transition_spinner.setSelection(spinner_3_position);
        
        
        
}
	public void onPause() {
		super.onPause();
		SaveSettings();
		overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);

	
		
	}
	
	public void SaveSettings() {
		SharedPreferences.Editor preferencesEditor = preferences.edit();

		switch(transition_spinner.getSelectedItemPosition()) {
			case 0:
				MainActivity.transition_in_1 = R.anim.bottom_in;
				MainActivity.transition_out_1 = R.anim.bottom_out;
				MainActivity.transition_in_2 = R.anim.bottom_in2;
				MainActivity.transition_out_2 = R.anim.bottom_out2;
				preferencesEditor.putInt("transition_in_1", R.anim.bottom_in);
				preferencesEditor.putInt("transition_out_1", R.anim.bottom_out);
				preferencesEditor.putInt("transition_in_2", R.anim.bottom_in2);
				preferencesEditor.putInt("transition_out_2", R.anim.bottom_out2);
				preferencesEditor.putInt("spinner_3_position", transition_spinner.getSelectedItemPosition());
				break;
			case 1:
				MainActivity.transition_in_1 = R.anim.fade;
				MainActivity.transition_out_1 = R.anim.rev_fade;
				MainActivity.transition_in_2 = R.anim.fade;
				MainActivity.transition_out_2 = R.anim.rev_fade;
				preferencesEditor.putInt("transition_in_1", R.anim.fade);
				preferencesEditor.putInt("transition_out_1", R.anim.rev_fade);
				preferencesEditor.putInt("transition_in_2", R.anim.rev_fade);
				preferencesEditor.putInt("transition_out_2", R.anim.fade);
				preferencesEditor.putInt("spinner_3_position", transition_spinner.getSelectedItemPosition());
				break;
			case 2:
				MainActivity.transition_in_1 = R.anim.in;
				MainActivity.transition_out_1 = R.anim.out;
				MainActivity.transition_in_2 = R.anim.in2;
				MainActivity.transition_out_2 = R.anim.out2;
				preferencesEditor.putInt("transition_in_1", R.anim.in);
				preferencesEditor.putInt("transition_out_1", R.anim.out);
				preferencesEditor.putInt("transition_in_2", R.anim.in2);
				preferencesEditor.putInt("transition_out_2", R.anim.out2);
				preferencesEditor.putInt("spinner_3_position", transition_spinner.getSelectedItemPosition());
				break;
		}

		preferencesEditor.putString("font", FONTS[font_spinner.getSelectedItemPosition()]);
		preferencesEditor.putInt("spinner_1_position", font_spinner.getSelectedItemPosition());
		preferencesEditor.commit();
		ApplicationListGrid.set_pacs(true);
		
		
	}
	public void onBackPressed() {
		super.onBackPressed();
		SaveSettings();
		overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);


	}
	
}
