package com.baudelacity.lynn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PacReceiver extends BroadcastReceiver {
	Context context;
	PacReceiver(Context c) {
		context = c;
	}
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		ApplicationListGrid.set_pacs(false);
		context.unregisterReceiver(this);

	}

}
