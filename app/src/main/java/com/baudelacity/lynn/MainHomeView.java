package com.baudelacity.lynn;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class MainHomeView extends Fragment {
	ImageView option;
	TextView Day, Date, Widget_text;
	public static String date, day; //, PREFERENCES_NAME = "WID_ID";
	public static final short RESULT_OK = -1; //, RESULT_CANCELED = 0, MAX_VALUE = 600;;
	AppWidgetManager mAppWidgetManager;
	LauncherAppWidgetHost mAppWidgetHost;
	public static ViewGroup Widgets;
	public static int widget_ID[] = new int[10], numWidgets = -1;
	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	String Settings[];
	String date_top, date_bottom;
	int WidgetHeight[] = new int[10];
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_home_layout, container, false);

		final Context context = this.getActivity();
		Settings = context.getResources().getStringArray(R.array.TIME);
		option = (ImageView) view.findViewById(R.id.optons);
		Day = (TextView) view.findViewById(R.id.Day);
    	Date = (TextView) view.findViewById(R.id.Date);
    	Widget_text = (TextView) view.findViewById(R.id.widgets_text);
    	mAppWidgetManager = AppWidgetManager.getInstance(context);
    	mAppWidgetHost = new LauncherAppWidgetHost(context, R.id.APPWIDGET_HOST_ID);
		Widgets = (ViewGroup) view.findViewById(R.id.Widgets);
		LoadSettings();
		
		option.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				final Dialog dialog = new Dialog(getActivity());
				dialog.setContentView(R.layout.dialog_view);
				dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
				TextView header = (TextView) dialog.findViewById(R.id.dialog_header);
				Button Option1 = (Button) dialog.findViewById(R.id.option_1);
				Button Option2 = (Button) dialog.findViewById(R.id.option_2);
				Button Option3 = (Button) dialog.findViewById(R.id.option_3);
				Button Dismiss = (Button) dialog.findViewById(R.id.dismiss_button);
				ImageView icon = (ImageView) dialog.findViewById(R.id.dialog_icon);
				header.setText(getResources().getText(R.string.options));
				Dismiss.setText(getResources().getText(R.string.dismiss));
				dialog.getWindow().setDimAmount(0.85f);
				icon.setVisibility(View.GONE);

				Option1.setText(getResources().getText(R.string.add_wdgt));
				Option2.setText(getResources().getText(R.string.rem_wdgt));
				Option3.setText(getResources().getText(R.string.settings));


				Option1.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						selectWidget();
						dialog.dismiss();
					}
				});
				Option2.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(numWidgets!=-1) {
							Widgets.removeAllViews();
							preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
							SharedPreferences.Editor preferencesEditor = preferences.edit();

							for (short i=0; i <= numWidgets; ++i) {
								preferencesEditor.putInt("widget_"+ i, 0);
								preferencesEditor.putInt("numWidgets", -1);
							}
							preferencesEditor.commit();
							numWidgets = -1;
						} else {
						}
						dialog.dismiss();
					}
				});
				Option3.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(context, Settings.class);
						context.startActivity(intent);
						((Activity) context).overridePendingTransition(MainActivity.transition_in_1, MainActivity.transition_out_1);
						dialog.dismiss();
					}
				});
				Dismiss.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
				dialog.show();
				/*
				final CharSequence[] items = {
						getResources().getText(R.string.add_wdgt),
						getResources().getText(R.string.rem_wdgt),
						getResources().getText(R.string.settings),
						};
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Dialog);
				builder.setIcon(R.mipmap.ic_launcher);
				builder.setTitle(getResources().getText(R.string.options));
				builder.setItems(items, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int item) {
						switch(item) {
						case 0: {
							selectWidget();
		                    return;
						}
						case 1: {
							
									if(numWidgets!=-1) {
				        	    		Widgets.removeAllViews();
				        	    		preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
				        		    	SharedPreferences.Editor preferencesEditor = preferences.edit();
				       		    	
				       		    	for (short i=0; i <= numWidgets; ++i) {
				       		        	preferencesEditor.putInt("widget_"+ i, 0);
				       		        	preferencesEditor.putInt("numWidgets", -1);
				       		        }
				        		    	preferencesEditor.commit();
				        		    	numWidgets = -1;
				        	    	} else {
				        	    	}	
								return;

						}
						
						case 2: {
							
							Intent intent = new Intent(context, Settings.class);
							context.startActivity(intent);
							((Activity) context).overridePendingTransition(MainActivity.transition_in_1, MainActivity.transition_out_1);						}
						}
						
					}
				});

				AlertDialog alert = builder.create();
				alert.show();
				*/
			}
		});

		day = new SimpleDateFormat(date_top).format(Calendar.getInstance().getTime());
    	day = day.substring(0,1).toUpperCase() + day.substring(1).toLowerCase();
    	date = new SimpleDateFormat(date_bottom).format(Calendar.getInstance().getTime());
		date = date.substring(0,1).toUpperCase() + date.substring(1).toLowerCase();
    	Day.setText(day);
    	Date.setText(date);
		
		//=================================
    	
    	preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    	numWidgets = preferences.getInt("numWidgets", -1);
    	if(numWidgets != -1) {
    		for (short i=0; i <= numWidgets; ++i) {
    			String load = "widget_"+i;
				String load2 = "Widget_height_"+i;
    			widget_ID[i] = preferences.getInt(load, 0);
				WidgetHeight[i] = preferences.getInt(load2, 2);
    			restoreWidget(widget_ID[i], WidgetHeight[i]);
				Log.d("Widget", "Widget height " + WidgetHeight[i]);
    		}
    		} else {
    		}
 
	return view;
	}
	
	@Override
	public void onStart() {
        super.onStart();
        mAppWidgetHost.startListening();
    }
    @Override
	public void onStop() {
        super.onStop();
        mAppWidgetHost.stopListening();
    }
    
    public void onResume() {
    	super.onResume();
    	LoadSettings();
		Log.d("Widget height_onResume", "Widget height: " + WidgetHeight[0]);
		getActivity().overridePendingTransition(MainActivity.transition_in_2, MainActivity.transition_out_2);
    }

	void selectWidget() {
        int appWidgetId = this.mAppWidgetHost.allocateAppWidgetId();
        Intent pickIntent = new Intent(AppWidgetManager.ACTION_APPWIDGET_PICK);
        pickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        addEmptyData(pickIntent);
        startActivityForResult(pickIntent, R.id.REQUEST_PICK_APPWIDGET);
    }
	@SuppressWarnings("unchecked")
	void addEmptyData(Intent pickIntent) {
        @SuppressWarnings("rawtypes")
		ArrayList customInfo = new ArrayList();
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_INFO, customInfo);
        @SuppressWarnings("rawtypes")
		ArrayList customExtras = new ArrayList();
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_EXTRAS, customExtras);
    };
    
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	
    	if (requestCode!=2) {
        if (resultCode == RESULT_OK ) {
            if (requestCode == R.id.REQUEST_PICK_APPWIDGET) {
                configureWidget(data);
            }
            else if (requestCode == R.id.REQUEST_CREATE_APPWIDGET) {
                createWidget(data);
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED && data != null) {
            int appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
            if (appWidgetId != -1) {
                mAppWidgetHost.deleteAppWidgetId(appWidgetId);
            }
        }
    	}
    	}
    
    private void configureWidget(Intent data) {
        Bundle extras = data.getExtras();
        int appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        AppWidgetProviderInfo appWidgetInfo = mAppWidgetManager.getAppWidgetInfo(appWidgetId);
        if (appWidgetInfo.configure != null) {
            Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
            intent.setComponent(appWidgetInfo.configure);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            startActivityForResult(intent, R.id.REQUEST_CREATE_APPWIDGET);
        } else {
            createWidget(data);
        }
    }
    public void createWidget(Intent data) {
        Bundle extras = data.getExtras();
        int appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        AppWidgetProviderInfo appWidgetInfo = mAppWidgetManager.getAppWidgetInfo(appWidgetId);
        final LauncherAppWidgetHostView hostView = (LauncherAppWidgetHostView) mAppWidgetHost.createView(getActivity(), appWidgetId, appWidgetInfo);
        hostView.setAppWidget(appWidgetId, appWidgetInfo);
		int wynik = (appWidgetInfo.minHeight+30)/70;
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(appWidgetInfo.minWidth,appWidgetInfo.minHeight);
		Log.d("TEST", "Szerokość: "+ (appWidgetInfo.minWidth+30)/70 +", Wysokość: "+(appWidgetInfo.minHeight+30)/70);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade);
        hostView.startAnimation(animation);

        final SharedPreferences.Editor preferencesEditor = preferences.edit();

        numWidgets++;

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this.getActivity());
		helpBuilder.setTitle("Pop Up");
		final NumberPicker test = new NumberPicker(getActivity());
		test.setMinValue(1);
		test.setMaxValue(5);
		helpBuilder.setView(test);
		helpBuilder.setMessage("This is a Simple Pop Up");
		helpBuilder.setPositiveButton("Set",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						WidgetHeight[numWidgets] = test.getValue()+1;
						Widgets.addView(hostView, Widgets.getWidth(), 70*WidgetHeight[numWidgets] - 30);
					}
				});

		helpBuilder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do nothing
			}
		});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

		for (int i=0; i <= numWidgets; ++i) {
			widget_ID[numWidgets] = appWidgetId;
			preferencesEditor.putInt("widget_"+ numWidgets, widget_ID[numWidgets]);
			preferencesEditor.putInt("numWidgets", numWidgets);
			preferencesEditor.putInt("Widget_height_"+numWidgets, WidgetHeight[numWidgets]);
			preferencesEditor.commit();
		}



	}
    
    public void restoreWidget(int appWidgetId, int WidgetHeight) {
    	AppWidgetProviderInfo appWidgetInfo = mAppWidgetManager.getAppWidgetInfo(appWidgetId);
        LauncherAppWidgetHostView hostView = (LauncherAppWidgetHostView) mAppWidgetHost.createView(getActivity(), appWidgetId, appWidgetInfo);
        hostView.setAppWidget(appWidgetId, appWidgetInfo);
		Log.d("Widget height", "Widget_height: " + WidgetHeight);
        Widgets.addView(hostView, Widgets.getWidth(), 70*WidgetHeight - 30);
    }
    
    public void removeWidget(AppWidgetHostView hostView) {
	    mAppWidgetHost.deleteAppWidgetId(hostView.getAppWidgetId());
	    Widgets.removeView(hostView);
	}
    
    void LoadSettings() {
		preferences = super.getActivity().getSharedPreferences(ApplicationListGrid.PREFERENCES_NAME
				, Activity.MODE_PRIVATE);
    	Day.setTypeface(ApplicationListGrid.typeface);
    	Date.setTypeface(ApplicationListGrid.typeface);
    	Widget_text.setTypeface(ApplicationListGrid.typeface);
		date_top = preferences.getString("date_spinner_top", Settings[0]);
		date_bottom = preferences.getString("date_spinner_bottom", Settings[1]);

		day = new SimpleDateFormat(date_top).format(Calendar.getInstance().getTime());
		day = day.substring(0,1).toUpperCase() + day.substring(1).toLowerCase();
		date = new SimpleDateFormat(date_bottom).format(Calendar.getInstance().getTime());
		date = date.substring(0,1).toUpperCase() + date.substring(1).toLowerCase();
		Day.setText(day);
		Date.setText(date);


    }
	//public static boolean isAppInstalled(Context context, String packageName) {
	//	try {
	//		context.getPackageManager().getApplicationInfo(packageName, 0);
	//		return true;
	//	}
	//	catch (PackageManager.NameNotFoundException e) {
	//		return false;
	//	}
	//}
}
	



