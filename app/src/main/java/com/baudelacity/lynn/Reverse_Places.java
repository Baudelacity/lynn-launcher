package com.baudelacity.lynn;

/**
 * Created by Krzysztof on 13.06.2016.
 */
public class Reverse_Places {
    private SQL zb;
    private short oldID, newID;

    public void start(SQL zb, int oldID, int newID) {
        Icon_Handler a = zb.giveLauncher(oldID);
        Icon_Handler b = zb.giveLauncher(newID);

        String package_name, temp_package_name;
        String label, temp_label;
        String original_label, temp_original_label;
        String icon_hex, temp_icon_hex;
        String ooriginal_icon_hex, temp_original_icon_hex;

        temp_package_name = a.getPackage_name();
        temp_label = a.getLabel();
        temp_original_label = a.getOriginal_label();
        temp_icon_hex = a.getIcon_hex();
        temp_original_icon_hex = a.getOriginal_icon_hex();

        a.setPackage_name(b.getPackage_name());
        a.setLabel(b.getLabel());
        a.setOriginal_label(b.getOriginal_label());
        a.setIcon_hex(b.getIcon_hex());
        a.setOriginal_icon_hex(b.getOriginal_icon_hex());

        b.setPackage_name(temp_package_name);
        b.setLabel(temp_label);
        b.setOriginal_label(temp_original_label);
        b.setIcon_hex(temp_icon_hex);
        b.setOriginal_icon_hex(temp_original_icon_hex);

        zb.updateLauncher(a);
        zb.updateLauncher(b);
    }
    }

