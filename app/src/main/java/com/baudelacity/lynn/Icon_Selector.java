package com.baudelacity.lynn;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by Krzysztof on 25.08.2016.
 */
public class Icon_Selector extends Activity
    {
        int Settings [];
        Integer hex;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.icon_grid_layout);
        if (!MainActivity.isTablet(this)) {
            if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                //Adapter_and_handler();
            }
            //Log.d("IsTablet", "Yes, it's a tablet");

        }

            ImageView back_button = (ImageView) findViewById(R.id.back_button);
            back_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
                }
            });
        GridView list = (GridView) findViewById(R.id.content);
        final String[] DayOfWeek = {
                "Blue 1", "Blue 2", "Blue 3",
                "Blue 4", "Gray 1", "Blue 1",
                "Blue 2", "Blue 3","Blue 4",
                "Blue 2", "Blue 3","Blue 4",
                "Blue 1", "Blue 2", "Blue 3",
                "Blue 4", "Gray 1", "Blue 1",
                "Blue 2", "Blue 3","Blue 4",
                "Blue 2", "Blue 3","Blue 4",
                "Blue 2", "Blue 3","Blue 4",
                "Blue 2", "Blue 3","Blue 4",
                "Gray 1","Gray 1","Gray 1"
        };
            final Integer[] iconId = {
                    R.mipmap.account,R.mipmap.browser, R.mipmap.calculator,
                    R.mipmap.calendar, R.mipmap.camera, R.mipmap.clock,
                    R.mipmap.contact, R.mipmap.document, R.mipmap.download,
                    R.mipmap.drive,R.mipmap.facebook, R.mipmap.fb_messenger,
                    R.mipmap.file, R.mipmap.gallery, R.mipmap.gmail,
                    R.mipmap.google, R.mipmap.mail, R.mipmap.map,
                    R.mipmap.message, R.mipmap.music, R.mipmap.music_store,
                    R.mipmap.note, R.mipmap.phone, R.mipmap.rss,
                    R.mipmap.search, R.mipmap.setting, R.mipmap.sim,
                    R.mipmap.store, R.mipmap.terminal, R.mipmap.twitter,
                    R.mipmap.video, R.mipmap.weather, R.mipmap.youtube

            };
        BGPickerAdapter adapter = new BGPickerAdapter(Icon_Selector.this, DayOfWeek, iconId);
        list.setAdapter(adapter);

            if (MainActivity.isTablet(this)) {
                Settings = getResources().getIntArray(R.array.tablet);
            } else {
                Settings = getResources().getIntArray(R.array.phone);
            }

            GridView grid = (GridView) findViewById(R.id.content);
            grid.setNumColumns(Settings[0]);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                hex = position;
                Intent intent=new Intent();
                intent.putExtra("ICON",position);
                setResult(Activity.RESULT_OK,intent);
                Log.d("Value", Integer.toString(position));
                finish();
                overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
            }


        });



    }
        public void onBackPressed() {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);


        }
    }
