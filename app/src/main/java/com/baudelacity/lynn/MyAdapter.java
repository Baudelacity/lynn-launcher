package com.baudelacity.lynn;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

public class MyAdapter extends FragmentPagerAdapter {
	public MyAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public int getCount() {
		return 2;
	}


	@Override
	public Fragment getItem(int position) {
		
		switch (position) {
		case 0:
			return new MainHomeView();
			case 1:
				return new ApplicationListGrid();
			default:
			return null;
		}
	}
}

