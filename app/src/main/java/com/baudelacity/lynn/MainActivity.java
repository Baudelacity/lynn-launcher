package com.baudelacity.lynn;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;


public class MainActivity extends FragmentActivity {

    public static ViewPager HomeHandler;

    MyAdapter PageAdapter;
    Activity activity;
    SharedPreferences preferences;
    public static Integer transition_in_1, transition_in_2, transition_out_1, transition_out_2;

    void AssignGUIItemsToCode() {
        HomeHandler = (ViewPager) findViewById(R.id.HomePageHandler);
    }

    void Adapter_and_handler() {
        PageAdapter = new MyAdapter(getFragmentManager());
        HomeHandler.setAdapter(PageAdapter);
        HomeHandler.setCurrentItem(1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ChangeFont.replaceDefaultFont(this, "MONOSPACE", "fonts/nokiapurebold.ttf");
        ChangeFont.replaceDefaultFont(this, "SERIF", "fonts/nokiapure.ttf");
        ChangeFont.replaceDefaultFont(this, "SANS_SERIF", "fonts/nokiapurelight.ttf");
        setContentView(R.layout.activity_main);
        AssignGUIItemsToCode();

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences = this.getSharedPreferences(ApplicationListGrid.PREFERENCES_NAME, Activity.MODE_PRIVATE);
        transition_in_1 = preferences.getInt("transition_in_1", R.anim.bottom_in);
        transition_out_1 = preferences.getInt("transition_out_1", R.anim.bottom_out);
        transition_in_2 = preferences.getInt("transition_in_2", R.anim.bottom_in2);
        transition_out_2 = preferences.getInt("transition_out_2", R.anim.bottom_out2);

        final float w = (float) (getResources().getDisplayMetrics().density);

        if (!isTablet(this)) {
            if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                //Adapter_and_handler();
            }
            //Log.d("IsTablet", "Yes, it's a tablet");

        } else {
            //Log.d("IsTablet", "No, it's a phone");
        }
        Adapter_and_handler();
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);





    }




    public static boolean isTablet(Context c) {
        return (c.getResources().getConfiguration().screenLayout
            & Configuration.SCREENLAYOUT_SIZE_MASK)
            >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
    public void onBackPressed() {
    	
    }

    public void onResume() {
        super.onResume();
    }

}


    //=================================================================
    