package com.baudelacity.lynn;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

public class Settings extends Activity
{
	private ListView list ;  
	Context c;
    private ArrayAdapter<String> adapter ;  
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_categories_layout);

        c = getApplicationContext();
        list = (ListView) findViewById(R.id.List);

        String settings[] = {getResources().getString(R.string.general), getResources().getString(R.string.applications),getResources().getString(R.string.date), getResources().getString(R.string.about_blackmoor)};
        
        ArrayList<String> settingsArray = new ArrayList<String>();  
        settingsArray.addAll( Arrays.asList(settings) );  
 
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, settingsArray);
        list.setAdapter(adapter);

		ImageView back_button = (ImageView) findViewById(R.id.back_button);
		back_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
        
        list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				switch (position) {
				case 0:
					Intent intent = new Intent(view.getContext(), GeneralSettings.class);
					view.getContext().startActivity(intent);
					overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
					return;
				case 1:
					Intent intent2 = new Intent(view.getContext(), ApplicationSettings.class);
					view.getContext().startActivity(intent2);
					overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
					return;
				case 2:
					Intent intent3 = new Intent(view.getContext(), DateWeatherSettings.class);
					view.getContext().startActivity(intent3);
					overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
					return;
				case 3:
					Intent intent4 = new Intent(view.getContext(), AboutSettings.class);
					view.getContext().startActivity(intent4);
					overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
					return;
				}
				
				
			}


        	
        });




	}



	
}
