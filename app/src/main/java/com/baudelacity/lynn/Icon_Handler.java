package com.baudelacity.lynn;

import android.graphics.drawable.Drawable;

/**
 * Created by Krzysztof on 13.06.2016.
 */
public class Icon_Handler {
        private Long nr;
        private String package_name;
        private String label;
        private String original_label;
        private String icon_hex;
        private String original_icon_hex;
        private Drawable icon;
        private Drawable original_icon;

        public void setNr(Long nr) {
            this.nr = nr;
        }
        public Long getNr() {
            return nr;
        }
        public void setPackage_name(String package_name) {
            this.package_name = package_name;
        }
        public String getPackage_name() {
            return package_name;
        }
        public void setLabel(String label) {
            this.label = label;
        }
        public String getLabel() {
            return label;
        }
        public void setIcon_hex(String icon_hex) {
            this.icon_hex= icon_hex;
        }
        public String getIcon_hex() {
            return icon_hex;
        }
        public void setOriginal_label(String original_label) {
            this.original_label=original_label;
        }
        public String getOriginal_label() {
            return original_label;
        }
        public void setOriginal_icon_hex(String original_icon_hex) {
            this.original_icon_hex = original_icon_hex;
        }
        public String getOriginal_icon_hex() {
            return original_icon_hex;
        }
        public void setIcon(Drawable icon) {
            this.icon = icon;
        }
        public Drawable getIcon() {
            return icon;
        }
        public void setOriginal_icon(Drawable original_icon) {
        this.original_icon = original_icon;
    }
        public Drawable getOriginal_icon() {
        return original_icon;
    }
        public boolean isLabelSmallerThan(Icon_Handler aPac) {
        return this.label.compareToIgnoreCase(aPac.label)>0;
    }
    }

