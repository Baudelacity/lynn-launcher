package com.baudelacity.lynn;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

public class icon_editor extends Activity {

    final Integer[] imageId = {
            R.mipmap.material_amber_1, R.mipmap.material_amber_2, R.mipmap.material_amber_3, R.mipmap.material_amber_4, R.mipmap.material_amber_5,
            R.mipmap.material_blue_1, R.mipmap.material_blue_2, R.mipmap.material_blue_3, R.mipmap.material_blue_4, R.mipmap.material_blue_5,
            R.mipmap.material_blue_grey_1, R.mipmap.material_blue_grey_2, R.mipmap.material_blue_grey_3, R.mipmap.material_blue_grey_4, R.mipmap.material_blue_grey_5,
            R.mipmap.material_brown_1, R.mipmap.material_brown_2, R.mipmap.material_brown_3, R.mipmap.material_brown_4, R.mipmap.material_brown_5,
            R.mipmap.material_cyan_1, R.mipmap.material_cyan_2, R.mipmap.material_cyan_3, R.mipmap.material_cyan_4, R.mipmap.material_cyan_5,
            R.mipmap.material_deep_orange_1, R.mipmap.material_deep_orange_2, R.mipmap.material_deep_orange_3, R.mipmap.material_deep_orange_4, R.mipmap.material_deep_orange_5,
            R.mipmap.material_deep_purple_1, R.mipmap.material_deep_purple_2, R.mipmap.material_deep_purple_3, R.mipmap.material_deep_purple_4, R.mipmap.material_deep_purple_5,
            R.mipmap.material_green_1, R.mipmap.material_green_2, R.mipmap.material_green_3, R.mipmap.material_green_4, R.mipmap.material_green_5,
            R.mipmap.material_grey_1, R.mipmap.material_grey_2, R.mipmap.material_grey_3, R.mipmap.material_grey_4, R.mipmap.material_grey_5,
            R.mipmap.material_indigo_1, R.mipmap.material_indigo_2, R.mipmap.material_indigo_3, R.mipmap.material_indigo_4, R.mipmap.material_indigo_5,
            R.mipmap.material_light_blue_1, R.mipmap.material_light_blue_2, R.mipmap.material_light_blue_3, R.mipmap.material_light_blue_4, R.mipmap.material_light_blue_5,
            R.mipmap.material_light_green_1, R.mipmap.material_light_green_2, R.mipmap.material_light_green_3, R.mipmap.material_light_green_4, R.mipmap.material_light_green_5,
            R.mipmap.material_lime_1, R.mipmap.material_lime_2, R.mipmap.material_lime_3, R.mipmap.material_lime_4, R.mipmap.material_lime_5,
            R.mipmap.material_orange_1, R.mipmap.material_orange_2, R.mipmap.material_orange_3, R.mipmap.material_orange_4, R.mipmap.material_orange_5,
            R.mipmap.material_pink_1, R.mipmap.material_pink_2, R.mipmap.material_pink_3, R.mipmap.material_pink_4, R.mipmap.material_pink_5,
            R.mipmap.material_purple_1, R.mipmap.material_purple_2, R.mipmap.material_purple_3, R.mipmap.material_purple_4, R.mipmap.material_purple_5,
            R.mipmap.material_red_1, R.mipmap.material_red_2, R.mipmap.material_red_3, R.mipmap.material_red_4, R.mipmap.material_red_5,
            R.mipmap.material_teal_1, R.mipmap.material_teal_2, R.mipmap.material_teal_3, R.mipmap.material_teal_4, R.mipmap.material_teal_5,
            R.mipmap.material_yellow_1, R.mipmap.material_yellow_2, R.mipmap.material_yellow_3, R.mipmap.material_yellow_4, R.mipmap.material_yellow_5,

    };
    final Integer[] listicons = {
            R.mipmap.account,R.mipmap.browser, R.mipmap.calculator,
            R.mipmap.calendar, R.mipmap.camera, R.mipmap.clock,
            R.mipmap.contact, R.mipmap.document, R.mipmap.download,
            R.mipmap.drive,R.mipmap.facebook, R.mipmap.fb_messenger,
            R.mipmap.file, R.mipmap.gallery, R.mipmap.gmail,
            R.mipmap.google, R.mipmap.mail, R.mipmap.map,
            R.mipmap.message, R.mipmap.music, R.mipmap.music_store,
            R.mipmap.note, R.mipmap.phone, R.mipmap.rss,
            R.mipmap.search, R.mipmap.setting, R.mipmap.sim,
            R.mipmap.store, R.mipmap.terminal, R.mipmap.twitter,
            R.mipmap.video, R.mipmap.weather, R.mipmap.youtube

    };

    private Uri picUri;



    Bitmap mainImage, Icon, scaled_icon, mask, overlay, highlight, loaded_icon, result;
    Boolean boolean_custom_icon = false, maskIsChecked = false, frameIsChecked= false;
    RadioButton rbtn, rbtn2, rbtn3;
    static String generatedIconHex;
    Drawable fullScaleIcon;
    public static Drawable AppIcon,  EditedAppIcon;
    public static String font, AppLabel, PackageName, OriginalAppLabel;
    Spinner position_spinner;
    TextView checkbox_header;
    ImageView icon;
    Bitmap icon_ready;
    protected static final int PICK_IMAGE = 0, PICK_BACKGROUND = 1, PICK_ICON = 2, CROP_PIC = 3;
    Button button_edit, button_whole;
    Bitmap Background = null;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_editor_2);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (!MainActivity.isTablet(this)) {
            if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                //Adapter_and_handler();
            }
            //Log.d("IsTablet", "Yes, it's a tablet");

        }

        icon = (ImageView) findViewById(R.id.icon_image);
        final EditText name = (EditText) findViewById(R.id.app_name);
        final TextView position_text = (TextView) findViewById(R.id.position_textview);
        ImageView back_button = (ImageView) findViewById(R.id.back_button);
        //position_text.setText(getResources().getText(R.string.position_1)+ " " + Integer.parseInt(String.valueOf(ApplicationListGrid.selected_ID+1))+". "+ getResources().getText(R.string.position_2)+".");
        position_text.setText(getResources().getText(R.string.position_1));
        AppIcon = ApplicationListGrid.AppIcon;
        EditedAppIcon = ApplicationListGrid.EditedAppIcon;
        PackageName = ApplicationListGrid.PackageName;
        final CheckBox filters_chechbox = (CheckBox) findViewById(R.id.filters_checkbox);
        final CheckBox filters_chechbox2 = (CheckBox) findViewById(R.id.Mask_checkbox);
        checkbox_header = (TextView) findViewById(R.id.checkbox_header);
        position_spinner = (Spinner) findViewById(R.id.position_spinner);

        AppLabel = DrawerLongClickListener.applabel;
        OriginalAppLabel = ApplicationListGrid.OriginalAppLabel;
        icon.setImageDrawable(EditedAppIcon);
        name.setText(AppLabel);
        name.setHint(OriginalAppLabel);
        name.setImeOptions(EditorInfo.IME_ACTION_DONE);
        generatedIconHex = null;


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        position_spinner.setAdapter(spinnerAdapter);

        for (short x = 0; x<ApplicationListGrid.list_size; x++) {
            spinnerAdapter.add(Integer.toString(x+1));
        }
        position_spinner.setSelection(ApplicationListGrid.selected_ID);

        spinnerAdapter.notifyDataSetChanged();





        Button button_save = (Button) findViewById(R.id.button_save);
        button_edit = (Button) findViewById(R.id.button_edit_icon);

        button_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(rbtn2.isChecked()) {
                    boolean_custom_icon=true;
                    maskIsChecked=true;
                    frameIsChecked=true;
                    Intent background_pick=new Intent(icon_editor.this,BackgroundSelector.class);
                    startActivityForResult(background_pick, PICK_BACKGROUND);
                    overridePendingTransition(R.anim.slide_in,R.anim.slide_out);

                }
                if(rbtn.isChecked()) {
                    boolean_custom_icon=false;

                    if(filters_chechbox.isChecked()) {
                        frameIsChecked=true;
                    } else {
                        frameIsChecked=false;
                    }
                    if(filters_chechbox2.isChecked()) {
                        maskIsChecked = true;
                    } else {
                        maskIsChecked = false;
                    }

                    intentstarter();
                }
                if(rbtn3.isChecked()) {
                    boolean_custom_icon=false;
                    maskIsChecked=false;
                    frameIsChecked=false;
                    Intent icon_pick=new Intent(icon_editor.this,Icon_Selector.class);
                    startActivityForResult(icon_pick, PICK_ICON);
                    overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                }


            }
        });
        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQL zb = new SQL(getApplicationContext());
                Icon_Handler a = zb.giveLauncher(ApplicationListGrid.selected_ID+1);

                String AppLabel = name.getText().toString();
                if(AppLabel.isEmpty()) {
                    AppLabel = OriginalAppLabel;
                }
                a.setLabel(AppLabel);
                if(generatedIconHex!= null) {
                    a.setIcon_hex(generatedIconHex);
                }

                zb.updateLauncher(a);
                new Reverse_Places().start(zb, ApplicationListGrid.selected_ID+1, Integer.parseInt(position_spinner.getSelectedItem().toString()));
                ApplicationListGrid.set_pacs(true);
                finish();
            }
        });

        checked_value();
    }
    void checked_value() {

        final CheckBox chb = (CheckBox) findViewById(R.id.filters_checkbox);
        final CheckBox chb2 = (CheckBox) findViewById(R.id.Mask_checkbox);
        RadioGroup group = (RadioGroup) findViewById(R.id.radioGroup);
        rbtn = (RadioButton) findViewById(R.id.rdb_custom);
        rbtn2 = (RadioButton) findViewById(R.id.Option_1);
        rbtn3 = (RadioButton) findViewById(R.id.stock_icon);

        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.Option_1:
                        button_edit.setText(getResources().getText(R.string.edit));
                        if(chb.isChecked()) {
                            chb.setChecked(false);
                            chb2.setChecked(false);
                            boolean_custom_icon=false;

                            maskIsChecked=false;
                            frameIsChecked=false;
                        }
                        if (chb2.isChecked()) {
                            chb.setChecked(false);
                            chb2.setChecked(false);
                            boolean_custom_icon=false;

                            maskIsChecked=false;
                            frameIsChecked=false;
                        }
                        chb.setEnabled(false);
                        chb2.setEnabled(false);
                        checkbox_header.setEnabled(false);

                        break;
                    case R.id.rdb_custom:
                        button_edit.setText(getResources().getText(R.string.edit_icon));
                        chb.setEnabled(true);
                        chb2.setEnabled(true);
                        checkbox_header.setEnabled(true);
                        boolean_custom_icon=false;

                        break;
                    case R.id.stock_icon:
                        boolean_custom_icon=true;
                        if(chb.isChecked()) {
                            chb.setChecked(false);
                            chb2.setChecked(false);
                            checkbox_header.setEnabled(false);
                            boolean_custom_icon=false;

                            maskIsChecked=false;
                            frameIsChecked=false;
                        }
                        if(chb2.isChecked()) {
                            chb.setChecked(false);
                            chb2.setChecked(false);
                            checkbox_header.setEnabled(false);
                            boolean_custom_icon=false;

                            maskIsChecked=false;
                            frameIsChecked=false;
                        }
                        chb.setEnabled(false);
                        chb2.setEnabled(false);
                        button_edit.setText(getResources().getText(R.string.stock_button));
                }
            }
        });
    }
    Drawable icon_generator_main(Drawable icon, int i, Boolean isStockIcon) {

        import_to_generator(icon, i, isStockIcon);
        create_scaled_icon(boolean_custom_icon, loaded_icon, mask, isStockIcon);
        Canvas canvas = new Canvas();
        canvas.setBitmap(result);
        Paint paint = new Paint();
        canvas.save();
        icon_setXfermode(paint, boolean_custom_icon, canvas );
        canvas.restore();
        Drawable d = new BitmapDrawable(result);
        return d;
    }
    void import_to_generator(Drawable icon, int i, Boolean isStockIcon) {
            mainImage = BitmapFactory.decodeResource(getResources(),
                    imageId[i]);
        mask= BitmapFactory.decodeResource(getResources(),
                R.mipmap.mask);
        overlay = BitmapFactory.decodeResource(getResources(),
                R.mipmap.overlay);
        highlight = BitmapFactory.decodeResource(getResources(),
                R.mipmap.highlight);
    if(isStockIcon==false) {
        loaded_icon = ((BitmapDrawable) icon).getBitmap(); } else
    {
        loaded_icon = BitmapFactory.decodeResource(getResources(),
                listicons[i]); }

        result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Bitmap.Config.ARGB_8888);
        Background = Bitmap.createScaledBitmap(mainImage, highlight.getWidth(), highlight.getHeight(),true);

    }
    Bitmap create_scaled_icon(boolean isChecked, Bitmap loaded_icon, Bitmap mask, Boolean isStockIcon) {

    if (isChecked) {
        scaled_icon = Bitmap.createScaledBitmap(loaded_icon, loaded_icon.getWidth(), loaded_icon.getHeight(), true);
    } else {
        scaled_icon = Bitmap.createScaledBitmap(loaded_icon, mask.getWidth(), mask.getHeight(), true);
    }
        return scaled_icon;
    }
    void icon_setXfermode(Paint paint, boolean IsChecked, Canvas canvas ) {
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
        if (IsChecked) {

            canvas.drawBitmap(Background, 0, 0,paint);
            paint.setXfermode(null);
            int width = Background.getWidth();
            int height = Background.getHeight();
            float centerX = (width  - scaled_icon.getWidth()) * 0.5f;
            float centerY = (height- scaled_icon.getHeight()) * 0.5f;



            canvas.drawBitmap(scaled_icon, centerX, centerY,paint);
        } else {
            canvas.drawBitmap(scaled_icon, 0, 0,paint);
        }
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(mask, 0, 0,paint);
        if (frameIsChecked) {
            Log.e("maskIsChecked", "TRUE");
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
            canvas.drawBitmap(highlight, 0, 0, paint);
        }
        if (maskIsChecked) {
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));
            Log.e("maskIsChecked", "FALSE");
            canvas.drawBitmap(overlay, 0, 0,paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
        }
        paint.setXfermode(null);
    }
    public static String encodeToBase64(Bitmap b) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOS);

        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }
    public static Drawable decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        Bitmap result = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        @SuppressWarnings("deprecation")
        Drawable d = new BitmapDrawable(result);
        return d;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_BACKGROUND) {
            if (resultCode == Activity.RESULT_OK) {
                Integer image_hex = data.getIntExtra("IMAGE", 0);
                Drawable icon_done = icon_generator_main(AppIcon, image_hex, false);

                icon_ready = ((BitmapDrawable) icon_done).getBitmap();
                icon.setImageBitmap(icon_ready);
                generatedIconHex = encodeToBase64(icon_ready);
            }

        }
        Log.d("RequestCode", Integer.toString(requestCode));
        if (requestCode == PICK_ICON) {
            if (resultCode == Activity.RESULT_OK) {
                Integer image_hex = data.getIntExtra("ICON", 0);
                Log.d("Result", Integer.toString(image_hex));
                Drawable icon_done = icon_generator_main(AppIcon, image_hex, true);

                icon_ready = ((BitmapDrawable) icon_done).getBitmap();
                icon.setImageBitmap(icon_ready);
                generatedIconHex = encodeToBase64(icon_ready);
            }

        }

        if (requestCode == PICK_IMAGE) {
            if (data == null) {
                return;
            }
            picUri = data.getData();
            performCrop();
        } else if (requestCode == CROP_PIC) {

            //Uri Selected_Image_Uri = data.getData();
            try {
                Bundle bundle = data.getExtras();
                Bitmap selectedBitmap = null;
                selectedBitmap= bundle.getParcelable("data");
                Drawable a = icon_generator_main(new BitmapDrawable(selectedBitmap), 5, false);
                Bitmap icon_ready = ((BitmapDrawable) a).getBitmap();
                icon.setImageBitmap(icon_ready);
                generatedIconHex = encodeToBase64(icon_ready);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }



        }
    }

    void intentstarter() {
        Intent intent_3 = new Intent();
        intent_3.setType("image/*");
        //intent_3.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(Intent.createChooser(intent_3, "Select Picture"), PICK_IMAGE);
        //Intent intent_3 = new Intent();
        //intent_3.setType("image/*");
        //intent_3.putExtra("crop", "true");
        //intent_3.putExtra("scale", "true");
        //intent_3.putExtra("rotate", "true");
        //intent_3.putExtra("aspectX",1);
        //intent_3.putExtra("aspectY",1);
        //intent_3.putExtra("outputX", 256);
        //intent_3.putExtra("outputY", 256);
        intent_3.setAction(Intent.ACTION_GET_CONTENT);
        //intent_3.putExtra("return-data", true);

        //intent_3.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(Intent.createChooser(intent_3, "Select Picture"), PICK_IMAGE);
    }

    private void performCrop() {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CROP_PIC);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast
                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}