package com.baudelacity.lynn;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class DrawerClickListener implements OnItemClickListener {
	static Context context;
	Icon_Handler[] handlerForAdapter;
	PackageManager pmForListener;
	Intent launchIntent;
	
	DrawerClickListener (Context c, Icon_Handler[] handler, PackageManager packageManager) {
		context = c;
		handlerForAdapter = handler;
		pmForListener = packageManager;
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		if (ApplicationListGrid.appLaunchable) {
			try {
				launchIntent = pmForListener.getLaunchIntentForPackage(handlerForAdapter[pos].getPackage_name());
				context.startActivity(launchIntent);
				((Activity) context).overridePendingTransition(MainActivity.transition_in_1, MainActivity.transition_out_1);
			} catch(NullPointerException e) {
			}
		}
	}

}
