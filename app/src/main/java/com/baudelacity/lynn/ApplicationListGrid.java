package com.baudelacity.lynn;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Random;


@SuppressLint("CutPasteId") public class ApplicationListGrid extends Fragment {

	static Context c;
	public static String font, AppLabel, PackageName, OriginalAppLabel;
	static GridView drawerGrid;
	public static Drawable AppIcon, EditedAppIcon;
	View view;
	static DrawerAdapter drawerAdapterObject;
	static Icon_Handler[] handler;
	static GridView AppGrid;
	static PackageManager packageManager;
	static boolean appLaunchable = true;
	public static int icon_size_value, text_size_value;
	//public static byte  selected_color;
	public static final String PREFERENCES_NAME = "Settings";
	public static SharedPreferences preferences;
	int column_width_value, column_width_value_land;
	static Typeface typeface;
	static List<ResolveInfo> pacsList;
	static Random r;
	private static boolean mask_boolean = true, icon_boolean = true;
	static String generatedIconHex, generatedOriginalIconHex;
	protected static final int PICK_IMAGE = 0;
	public static short selected_ID = 999;
	public static Boolean editor = false, FIRST_RUN = true;
	static int loaded_list_size;
	static int list_size, orig_list_size;
	static int count = 0;
	static int [] Settings;
	static final Integer[] imageId = {
			R.mipmap.material_amber_1,
			R.mipmap.material_blue_1,
			R.mipmap.material_blue_grey_1,
			R.mipmap.material_brown_1,
			R.mipmap.material_cyan_1,
			R.mipmap.material_deep_orange_1,
			R.mipmap.material_deep_purple_1,
			R.mipmap.material_green_1,
			R.mipmap.material_grey_1,
			R.mipmap.material_indigo_1,
			R.mipmap.material_light_blue_1,
			R.mipmap.material_light_green_1,
			R.mipmap.material_lime_1,
			R.mipmap.material_orange_1,
			R.mipmap.material_pink_1,
			R.mipmap.material_purple_1,
			R.mipmap.material_red_1,
			R.mipmap.material_teal_1,
			R.mipmap.material_yellow_1

	};
	static Bitmap Background = null;


	void AssignGUIItemsToCode() {

		AppGrid = (GridView) view.findViewById(R.id.content);
		drawerGrid = (GridView) view.findViewById(R.id.content);
		c = getActivity();
		packageManager = super.getActivity().getPackageManager();
		LoadSettings();
	}
	void Filters(IntentFilter filter) {
		filter.addAction(Intent.ACTION_PACKAGE_ADDED);
		filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
		filter.addDataScheme("package");
	}
	public static void set_pacs(boolean init) {
		SQL zb = new SQL(c);
		Drawable drawableAppIcon = null;
		SharedPreferences.Editor preferencesEdit = preferences.edit();
		FIRST_RUN = preferences.getBoolean("FIRST_RUN", true);
		final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		pacsList = packageManager.queryIntentActivities(mainIntent, 0);
		orig_list_size = pacsList.size();;
		if(FIRST_RUN==true) {
			list_size = pacsList.size();
			loaded_list_size = pacsList.size();
			preferencesEdit.putInt("list_size", loaded_list_size);
			preferencesEdit.commit();
		}
		else {
			list_size = zb.giveAll().size();
		}
		handler = new Icon_Handler[loaded_list_size];
		for (short i = 0; i<loaded_list_size; i++) {
			handler[i] = new Icon_Handler();
			if (FIRST_RUN == true) {
				handler[i].setNr((long) i);
				handler[i].setPackage_name(pacsList.get(i).activityInfo.packageName);
				handler[i].setLabel(pacsList.get(i).loadLabel(packageManager).toString());
				handler[i].setOriginal_label(pacsList.get(i).loadLabel(packageManager).toString());
				Random rand = new Random();
				int n = rand.nextInt(imageId.length);


				Bitmap original = ((BitmapDrawable) pacsList.get(i).loadIcon(packageManager)).getBitmap();
				Drawable icon_done = icon_gen(pacsList.get(i).loadIcon(packageManager), n);
				Bitmap icon_ready = ((BitmapDrawable) icon_done).getBitmap();
				generatedIconHex = encodeToBase64(icon_ready, pacsList.get(i).activityInfo.packageName);
				handler[i].setIcon_hex(generatedIconHex);
				handler[i].setOriginal_icon_hex(encodeToBase64(original,pacsList.get(i).activityInfo.packageName));
				zb.addLauncher(handler[i]);
			}
			int y = i + 1;
			try {
				handler[i] = zb.giveLauncher(y);
				AppLabel = handler[i].getLabel();
				OriginalAppLabel = handler[i].getOriginal_label();
				PackageName = handler[i].getPackage_name();
				generatedIconHex = handler[i].getIcon_hex();
				generatedOriginalIconHex = handler[i].getOriginal_icon_hex();
			} catch (IndexOutOfBoundsException e) {
			}

			handler[i].setPackage_name(PackageName);
			handler[i].setOriginal_label(OriginalAppLabel);
			handler[i].setLabel(AppLabel);
			handler[i].setIcon(decodeBase64(generatedIconHex));
			handler[i].setOriginal_icon(decodeBase64(generatedOriginalIconHex));
		}
		FIRST_RUN = false;
		preferencesEdit.putBoolean("FIRST_RUN", FIRST_RUN);
		preferencesEdit.commit();
		if (init) {
			drawerAdapterObject = new DrawerAdapter(c, handler);
			AppGrid.setAdapter(drawerAdapterObject);
			AppGrid.setOnItemClickListener(new DrawerClickListener(c, handler, packageManager));
			AppGrid.setOnItemLongClickListener(new DrawerLongClickListener(c, handler, packageManager));

		} else {
			drawerAdapterObject.notifyDataSetInvalidated();
		}
	}
	static void save(int a) {
		SharedPreferences.Editor preferencesEdit = preferences.edit();
		preferencesEdit.putInt("list_size", a);
		preferencesEdit.commit();
	}


	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.application_grid_layout, container, false);
		AssignGUIItemsToCode();
		LoadSettings();
		IntentFilter filter = new IntentFilter();
		Filters(filter);
		set_pacs(true);
		return view;
	}





	void intentstarter() {
		Intent intent_3 = new Intent();
		intent_3.setType("image/*");
		intent_3.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent_3, "Select Picture"), PICK_IMAGE);
		getActivity().overridePendingTransition(MainActivity.transition_in_1, MainActivity.transition_out_1);
	}
	public void onResume() {
		super.onResume();
		LoadSettings();
		final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		pacsList = packageManager.queryIntentActivities(mainIntent, 0);
		if(count <= 1) {
			if (loaded_list_size >= orig_list_size) {
				orig_list_size = loaded_list_size;
			} else {
				orig_list_size = pacsList.size();
			}
		} else {
			orig_list_size = pacsList.size();
		}
		SQL zb = new SQL(c);

		if (FIRST_RUN == false) {
			if (orig_list_size < zb.giveAll().size()) {
				try {
					remove_and_move(zb, selected_ID);

				} catch (CursorIndexOutOfBoundsException e) {
				}
				set_pacs(true);
			} else if (orig_list_size == zb.giveAll().size()) {
				//AddIcon_2(zb);
			} else if (orig_list_size > zb.giveAll().size()) {
				AddIcon_2(zb);
				//AddIcon(zb);

				set_pacs(true);
			}

		}
		count++;

	}
	void Toast(String string) {
		Toast.makeText(c, string, Toast.LENGTH_SHORT).show();
	}
	public void onPause() {
		super.onPause();
		LoadSettings();
	}
	public void onStart() {
		super.onStart();

		LoadSettings();
	}
	public void onDestroy() {
		super.onDestroy();
		SQL db = new SQL(c);
		db.close();
	}
	public void AddIcon(SQL zh){

		for(int x = list_size; x<orig_list_size;x++) {
			Icon_Handler i = new Icon_Handler();
			i.setPackage_name(pacsList.get(x).activityInfo.packageName);
			i.setLabel(pacsList.get(x).loadLabel(packageManager).toString());
			i.setOriginal_label(pacsList.get(x).loadLabel(packageManager).toString());
			Bitmap original = ((BitmapDrawable) pacsList.get(x).loadIcon(packageManager)).getBitmap();
			Random rand = new Random();
			int n = rand.nextInt(imageId.length);
			Drawable icon_done = icon_gen(pacsList.get(x).loadIcon(packageManager), n);
			Bitmap icon_ready = ((BitmapDrawable) icon_done).getBitmap();
			generatedIconHex = encodeToBase64(icon_ready, pacsList.get(x).activityInfo.packageName);
			i.setIcon_hex(generatedIconHex);
			i.setOriginal_icon_hex(encodeToBase64(original, pacsList.get(x).activityInfo.packageName));
			zh.addLauncher(i);
			loaded_list_size++;
			save(loaded_list_size);
			set_pacs(true);

		}


	}
	public void AddIcon_2(SQL zh) {
		for(int x = 1; x<orig_list_size; x++) {
			try {
			if(zh.searchKeyString(pacsList.get(x).activityInfo.packageName)!=true) {
				Icon_Handler i = new Icon_Handler();
				i.setPackage_name(pacsList.get(x).activityInfo.packageName);
				i.setLabel(pacsList.get(x).loadLabel(packageManager).toString());
				i.setOriginal_label(pacsList.get(x).loadLabel(packageManager).toString());
				Bitmap original = ((BitmapDrawable) pacsList.get(x).loadIcon(packageManager)).getBitmap();
				Random rand = new Random();
				int n = rand.nextInt(imageId.length);
				Drawable icon_done = icon_gen(pacsList.get(x).loadIcon(packageManager), n);
				Bitmap icon_ready = ((BitmapDrawable) icon_done).getBitmap();
				generatedIconHex = encodeToBase64(icon_ready, pacsList.get(x).activityInfo.packageName);
				i.setIcon_hex(generatedIconHex);
				i.setOriginal_icon_hex(encodeToBase64(original, pacsList.get(x).activityInfo.packageName));
				zh.addLauncher(i);
				loaded_list_size++;
				save(loaded_list_size);
				set_pacs(true);
			} else {
			} } catch (RuntimeException e) {
				Log.e("Error", "0x01");
			}
		}
	}


	public static boolean isSimSupport(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
		return !(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT);

	}
	public static void remove_and_move(SQL zh, int ID) {
		int x = ID+1;
		while (x <= zh.giveAll().size()) {
			if (x != zh.giveAll().size()) {
				Icon_Handler a = zh.giveLauncher(x);
				Icon_Handler b = zh.giveLauncher(x+1);
				a.setPackage_name(b.getPackage_name());
				a.setLabel(b.getLabel());
				a.setOriginal_label(b.getOriginal_label());
				a.setIcon_hex(b.getIcon_hex());
				a.setOriginal_icon_hex(b.getOriginal_icon_hex());
				zh.updateLauncher(a);
			} else {
				zh.deleteLauncher(zh.giveAll().size());
				loaded_list_size--;
				set_pacs(true);
			}
			x++;

			set_pacs(true);
		}

		save(loaded_list_size);


	}
	public void LoadSettings() {
		preferences = super.getActivity().getSharedPreferences(PREFERENCES_NAME, Activity.MODE_PRIVATE);
		if (MainActivity.isTablet(c)) {
			Settings = c.getResources().getIntArray(R.array.tablet);
		} else {
			Settings = c.getResources().getIntArray(R.array.phone);
		}

		int w = (int) (Settings[3] * getResources().getDisplayMetrics().density + 0.5f);
		drawerGrid.setVerticalSpacing(w);
		column_width_value = preferences.getInt("column_width_value", Settings[0]);
		column_width_value_land = preferences.getInt("column_width_value_land", Settings[1]);
		icon_size_value = preferences.getInt("icon_size_value", Settings[2]);
		text_size_value = preferences.getInt("text_size_value", Settings[3]);
		loaded_list_size = preferences.getInt("list_size", 0);
		font = preferences.getString("font", "fonts/nokiapure.ttf");
		typeface = Typeface.createFromAsset(getActivity().getAssets(), font);

		if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
			drawerGrid.setNumColumns(column_width_value);
		} else {
			drawerGrid.setNumColumns(column_width_value_land);
		}
	}
	public static boolean isAppInstalled(Context context, String packageName) {
		try {
			context.getPackageManager().getApplicationInfo(packageName, 0);
			return true;
		}
		catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}
	static Drawable icon_gen(Drawable icon, int i) {
		Canvas canvas = new Canvas();
		Bitmap scaled_icon;
		Bitmap mainImage = BitmapFactory.decodeResource(c.getResources(),
				imageId[i]);
		Bitmap mask = BitmapFactory.decodeResource(c.getResources(),
				R.mipmap.mask);
		Bitmap overlay = BitmapFactory.decodeResource(c.getResources(),
				R.mipmap.overlay);
		Bitmap highlight = BitmapFactory.decodeResource(c.getResources(),
				R.mipmap.highlight);
		Bitmap loaded_icon = ((BitmapDrawable) icon).getBitmap();
		Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Bitmap.Config.ARGB_8888);

		Background = Bitmap.createScaledBitmap(mainImage, highlight.getWidth(), highlight.getHeight(),true);

		if (icon_boolean == true) {
			scaled_icon = Bitmap.createScaledBitmap(loaded_icon, loaded_icon.getWidth(), loaded_icon.getHeight(), true);
		} else {
			scaled_icon = Bitmap.createScaledBitmap(loaded_icon, mask.getWidth(), mask.getHeight(), true);
		}


		canvas.setBitmap(result);
		Paint paint = new Paint();
		canvas.save();
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
		if (icon_boolean == true) {
			canvas.drawBitmap(Background, 0, 0,paint);
			paint.setXfermode(null);
			int width = Background.getWidth();
			int height = Background.getHeight();
			float centerX = (width  - scaled_icon.getWidth()) * 0.5f;
			float centerY = (height- scaled_icon.getHeight()) * 0.5f;



			//canvas.drawBitmap(scaled_icon, 25, 25,paint);
			canvas.drawBitmap(scaled_icon, centerX, centerY,paint);
		} else {
			canvas.drawBitmap(scaled_icon, 0, 0, paint);
		}
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
		canvas.drawBitmap(mask, 0, 0, paint);
		if (mask_boolean == true) {
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
			canvas.drawBitmap(highlight, 0, 0, paint);
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));
			canvas.drawBitmap(overlay, 0, 0, paint);
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
		}
		paint.setXfermode(null);

		canvas.restore();
		Drawable d = new BitmapDrawable(result);
		return d;
	}
	public static String encodeToBase64(Bitmap b, String packageName) {
		ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
		b.compress(CompressFormat.PNG, 100, byteArrayOS);
		return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
	}
	public static Drawable decodeBase64(String input) {
		byte[] decodedBytes = Base64.decode(input, 0);
		Bitmap result = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
		@SuppressWarnings("deprecation")
		Drawable d = new BitmapDrawable(result);
		return d;
	}



}