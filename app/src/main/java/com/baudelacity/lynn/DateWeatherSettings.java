package com.baudelacity.lynn;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Krzysztof on 22.08.2016.
 */

public class DateWeatherSettings extends Activity {

    private List<String> date_list_top, date_list_bottom;
    Spinner date_spinner_top, date_spinner_bottom;
    static int [] Settings;
    static String [] date_top, date_bottom;
    int spinner_1_position, spinner_2_position;
    SharedPreferences preferences;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = this.getSharedPreferences(ApplicationListGrid.PREFERENCES_NAME, Activity.MODE_PRIVATE);
        setContentView(R.layout.date_and_weather_settings);
        Settings = getResources().getIntArray(R.array.universal);
        date_top = getResources().getStringArray(R.array.TIME_TOP);
        date_bottom = getResources().getStringArray(R.array.TIME_BOTTOM);
        ImageView back_button = (ImageView) findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);
            }
        });
        date_spinner_top = (Spinner) findViewById(R.id.spinner_date_top);
        date_spinner_bottom= (Spinner) findViewById(R.id.spinner_date_bottom);


        spinner_1_position = preferences.getInt("date_spinner_top_position", Settings[0]);
        spinner_2_position = preferences.getInt("date_spinner_bottom_position", Settings[1]);

        date_list_top = new ArrayList<String>();

        for(int x=0;x<date_top.length;x++) {
            date_list_top.add(date_values(date_top[x]));
        }
        date_list_bottom = new ArrayList<String>();
        for(int x=0;x<date_bottom.length;x++) {
            date_list_bottom.add(date_values(date_bottom[x]));
        }


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item, date_list_top);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        date_spinner_top.setAdapter(dataAdapter);
        date_spinner_top.setSelection(spinner_1_position);

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item, date_list_bottom);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        date_spinner_bottom.setAdapter(dataAdapter2);
        date_spinner_bottom.setSelection(spinner_2_position);

    }

    String date_values(String value) {
        String result = "";
        result = new SimpleDateFormat(value).format(Calendar.getInstance().getTime());
        result = result.substring(0,1).toUpperCase() + result.substring(1).toLowerCase();
        return result;
    }

    public void SaveSettings() {
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putString("date_spinner_top", date_top[date_spinner_top.getSelectedItemPosition()]);
        preferencesEditor.putString("date_spinner_bottom", date_bottom[date_spinner_bottom.getSelectedItemPosition()]);
        preferencesEditor.putInt("date_spinner_top_position", date_spinner_top.getSelectedItemPosition());
        preferencesEditor.putInt("date_spinner_bottom_position", date_spinner_bottom.getSelectedItemPosition());
        preferencesEditor.commit();
        ApplicationListGrid.set_pacs(true);
    }

    public void onPause() {
        super.onPause();
        SaveSettings();
    }
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_2,R.anim.slide_in_2);


    }
}
